﻿import { VFAOPage } from './app.po';

describe('abp-zero-template App', function () {
    let page: VFAOPage;

    beforeEach(() => {
        page = new VFAOPage();
    });

    it('should display message saying app works', () => {
        page.navigateTo();
        page.getCopyright().then(value => {
            expect(value).toEqual(new Date().getFullYear() + ' © VFAO.');
        });
    });
});
